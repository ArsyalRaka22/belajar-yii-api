<?php

namespace app\controllers\api;

use app\components\NodeLogger;
use app\models\Buku;
use app\models\Genre;
use yii\web\NotFoundHttpException;
use Yii;

class GenreController extends RestController
{
    public function actionList()
    {
        return $this->output(Genre::find()->all());
    }

    public function actionDetail($id)
    {
        $model = $this->findModel($id);
        return $this->output($model);
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;

        $genre = new Genre();
        $genre->name = $request->post('name');
        $genre->save();

        return $this->output($genre);
    }

    public function actionUpdate($id)
    {
        $body = $this->getRawBody();

        $model = $this->findModel($id);
        $model->name = $body['name'];
        if ($model->save()) {
            return $this->output(['data' => 'oke']);
        }
        return $this->output([
            "error" => "id not found",
        ], 401);
    }

    public function actionDelete($id)
    {
        $id = $this->findModel($id);

        if ($id->delete()) {
            return $this->output(['data' => 'Success Delete Data']);
        }
        return $this->output(['data' =>  'Error Delete Data']);
    }

    protected function findModel($id)
    {
        if (($model = Genre::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
