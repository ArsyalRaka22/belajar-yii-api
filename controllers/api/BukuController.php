<?php

namespace app\controllers\api;

use app\components\NodeLogger;
use app\models\Buku;
use yii\web\NotFoundHttpException;
use Yii;

class BukuController extends RestController
{
    public function actionList()
    {
        return $this->output(Buku::find()->all());
    }

    public function actionDetail($id)
    {
        $model = $this->findModel($id);
        return $this->output($model);
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;

        $buku = new Buku();
        $buku->judul = $request->post('judul');
        $buku->namaPengarang = $request->post('namaPengarang');
        $buku->genreId = $request->post('genreId');
        $buku->save();

        return $this->output($buku);
    }

    public function actionUpdate($id)
    {
        $body = $this->getRawBody();

        $model = $this->findModel($id);
        $model->judul = $body['judul'];
        $model->namaPengarang = $body['namaPengarang'];
        $model->genreId = $body['genreId'];
        if ($model->save()) {
            return $this->output(['data' => 'oke']);
        }
        return $this->output([
            "error" => "id not found",
        ], 401);
    }

    public function actionDelete($id)
    {
        $id = $this->findModel($id);

        if ($id->delete()) {
            return $this->output(['data' => 'Success Delete Data']);
        }
        return $this->output(['data' =>  'Error Delete Data']);
    }


    protected function findModel($id)
    {
        if (($model = Buku::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
