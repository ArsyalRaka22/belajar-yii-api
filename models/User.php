<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $token
 * @property string $createAt
 * @property string $updateAt
 * @property string|null $deleteAt
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'username', 'password', 'token'], 'required'],
            [['createAt', 'updateAt', 'deleteAt'], 'safe'],
            [['name'], 'string', 'max' => 16],
            [['username', 'password', 'token'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'username' => 'Username',
            'password' => 'Password',
            'token' => 'Token',
            'createAt' => 'Create At',
            'updateAt' => 'Update At',
            'deleteAt' => 'Delete At',
        ];
    }
}
