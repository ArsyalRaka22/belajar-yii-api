<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "buku".
 *
 * @property int $id
 * @property string $judul
 * @property string $nama pengarang
 * @property int $genreId
 *
 * @property Genre $genre
 */
class Buku extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'buku';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul', 'namaPengarang', 'genreId'], 'required'],
            [['genreId'], 'integer'],
            [['judul', 'namaPengarang'], 'string', 'max' => 32],
            [['genreId'], 'exist', 'skipOnError' => true, 'targetClass' => Genre::class, 'targetAttribute' => ['genreId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'nama Pengarang' => 'Nama Pengarang',
            'genreId' => 'Genre ID',
        ];
    }

    public function fields()
    {
        return [
            "id",
            "judul",
            "namaPengarang",
            "genreId",
            "genre" => function () {
                return Genre::find()->where(["id" => $this->genreId])->one();
            }
        ];
    }


    /**
     * Gets query for [[Genre]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGenre()
    {
        return $this->hasOne(Genre::class, ['id' => 'genreId']);
    }
}
